*Schulmathematik* ist ein Bündelpaket für LaTeX, das Pakete und
Dokumentenklassen für deutschsprachige Mathematik- und Physiklehrer zur
Verfügung stellt.

Das Paket ist vom CTAN erhältlich: https://ctan.org/pkg/schulmathematik

Führen Sie `xelatex schulmathematik.tex` aus, um die Anleitung im PDF-Format zu
erhalten.

Die jüngsten Änderungen finden Sie am Ende der Anleitung.
